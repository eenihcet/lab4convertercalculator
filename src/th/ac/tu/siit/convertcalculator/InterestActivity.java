package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;

import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity 
implements OnClickListener{
	float interestRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);

		Button b1 = (Button)findViewById(R.id.button2);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.button1);
		b2.setOnClickListener(this);
		
		TextView tvRate = (TextView)findViewById(R.id.textView5);
		interestRate = Float.parseFloat(tvRate.getText().toString());
	}
	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.button2) {
			EditText dolla = (EditText)findViewById(R.id.editText1);
			EditText year = (EditText)findViewById(R.id.editText2);
			TextView tvTHB = (TextView)findViewById(R.id.textView3);
			String res = "";
			try {
				float m = Float.parseFloat(dolla.getText().toString());
				float y = Float.parseFloat(year.getText().toString());
				float sum = (float)(m * Math.pow((interestRate/100)+1, y));
				res = String.format(Locale.getDefault(), "%.2f", sum);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			tvTHB.setText(res.toString());
		}
		else if (id == R.id.button1) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("interests", interestRate);
			startActivityForResult(i, 9999);
			//9999 is a request code. It is a user-defined unique integer
			//It is used to identify the return value
		}
	}
	//Receive returned value

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			interestRate = data.getFloatExtra("editText1", 32.0f);
			TextView tvRate = (TextView)findViewById(R.id.tvRate);
			tvRate.setText(String.format(Locale.getDefault(), "%.2f", interestRate));
		}
	}

}
  